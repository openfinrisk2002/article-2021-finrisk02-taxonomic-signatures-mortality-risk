This directory contains ordination analysis (Bray-Curtis-PCoA and PCA) scripts.

# pca_all.R
Run all scripts in this folder

# set_up.R
Initialize output directory

# pca.R
Do PCA on CLR transformed species level data. Output in output/raw_output/