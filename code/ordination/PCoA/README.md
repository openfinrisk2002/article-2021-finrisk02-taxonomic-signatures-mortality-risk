This directory contains ordination analysis (Bray-Curtis-PCoA and PCA) scripts.

# set_up.R
Initialize output directory

# compute_braycurtis_pcoa_species.R
PCoA on species levels w Bray-Curtis dissimilarity. Output in output/raw_output/

# compute_braycurtis_species.R
Bray-Curtis dissimilarity matrix. Output in output/raw_output/