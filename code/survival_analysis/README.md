# main.R
Run entire survival analysis pipe line

# set_up.R
Loads packages and sets up output directory

# cox_settings.R
Cox regression covariates and other specifications. Output figure font.

# PCA/genus/subnet/functional_cox.R
Cox regression with prefix and the independent variable. Each script outputs the results in output/tables/

# survival_random_forest.R
Survival random forest with core/covariates/core + covariates as independent variables. Result table output in output/tables/

# survival_random_forest_cross_validation.R
Cross-validation for random forest with covariates as above. 

# Figures created as follows, saved in output/figures/

# create_figure_pca_mortality_association.R
Fig. 2: mortality ~ PCs

# create_figure_cause_of_death_enterobacteriaceae_continuous.R
Fig. 3: cause of death ~ Enterobacteriaceae

# create_figure_PC3_east_west_mortality.R
Supp. Fig. 2: PC3 in eastern/western Finland
  
# create_figure_PC_drivers.R
Supp. Fig. 3: PC 1-3 driver species

# create_figure_PC3_drivers_east_west.R
Supp. Fig. 4: PC 3 drivers in EAST/WEST

# create_figure_PC3_HR_cause_of_death_continuous.R
Supp. Fig. 5: cause of death ~ PC3

# create_figure_genus_panel_srf_importance.R"
Supp. Fig. 6: Individual genera and core importance
  