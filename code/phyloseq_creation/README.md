Scripts in this directory create phyloseq objects from the biom-files / Kraken-style SHOGUN report files.

# main.R
Sourcing this will run the all scripts in this filder

# code_phyloseq_feb16S_biom2phyloseq.R
Creates a phyloseq object from .biom, meta data and taxonomy files, removes plasmids and <50k read samples, aggregates taxa and saves the output to input/