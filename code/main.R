## Run the entire pipeline

## Set up paths
source("code/main_setup.R") 

## Phyloseq creation
source("code/phyloseq_creation/main.R")

## PCoA analyses
source("code/ordination/PCoA/braycurtis_pcoa_all.R") 
# see code/final/PCoA/readme.md for how to compute BrayCurtis efficiently on Atlas

## PCA
source("code/ordination/PCA/pca_all.R") 

## Network analysis
source("code/network_analysis/compute_network_all.R") 

## Survival analysis
source("code/survival_analysis/main.R") 

## Figure 1: population statistics, map, PCoA
source("code/create_figure1_map_and_pcoa/main.R")

## Supplementary figure 1: CLR abundance profile of top genera
source("code/create_Sfigure1_top_genera/FigS1.R")

## Supplementary figure 9: Enriched functional pathways
# Generated with the browser based FuncTree 2.1.9
# Details in code/create_Sfigure9_pathways/README.md

## Distribution tables for supplements
source("code/distribution_tables/compute_all.R")
