## Readme

installation instructions for SpiecEasi can be found at 
https://github.com/zdk123/SpiecEasi

# compute_network_all.R
sources all of the below

# network_functions.R 
All network related functions

# set_up.R
Load R packages and set output directories

# compute_spieceasi_analyses.R
Computes SpiecEasi analyses and saves results to output/raw_output/

# create_main_network_figure.R
Generates Fig. 4 with function main_subnet_analysis_figures() and saves the figures to output/figures/figure_subnets_shogun/

# create_supplementary_network_figure.R
Generates Supp. Figs 7 & 8 and saves the figures to output/figures/figure_subnets_shogun/

# write_spieceasi_tables.R
Produces subnet tables save in output/tables/spieceasi_tables/