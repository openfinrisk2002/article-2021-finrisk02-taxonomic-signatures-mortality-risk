Source code for the article

Taxonomic Signatures of Long-Term Mortality Risk in Human Gut Microbiota

Aaro Salosensaari, Ville Laitinen, Aki Havulinna, Guillaume Meric, Susan Cheng, Markus Perola, Liisa Valsta, Georg Alfthan, Michael Inouye, Jeramie D. Watrous, Tao Long, Rodolfo Salido, Karenina Sanders, Caitriona Brennan, Gregory C. Humphrey, Jon G. Sanders, Mohit Jain, Pekka Jousilahti, Veikko Salomaa, Rob Knight, Leo Lahti, Teemu Niiranen

medRxiv 2019.12.30.19015842; doi: https://doi.org/10.1101/2019.12.30.19015842 (preprint) 


## Instructions for running the pipeline

Source code for the subanalyses can be found in the folders under code/ and the README.md files describe purpose of the separate scripts. 

The entire pipeline can be run by 
1. creating a folder titled "input" and placing there the data files listed in code/main_setup.R
2. running code/main.R

The pipeline creates an output folder where all figures, tables and auxilary results are saved.  




